package com.ninestack.mvvm.network;

import com.ninestack.mvvm.login.UserModel;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ahmad Shubita on 8/1/17.
 */

public interface UsersService {
    @POST("/Karaoke_API/api/User/login")
    Observable<UserModel> login(@Body UserModel userData);
}
