package com.ninestack.mvvm.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ninestack.mvvm.MainActivity;
import com.ninestack.mvvm.R;
import com.ninestack.mvvm.databinding.ActivityLoginBinding;

import java.util.Observable;
import java.util.Observer;

public class LoginActivity extends AppCompatActivity implements LoginNavigator{
    private UserViewModel userViewModel;
    private ActivityLoginBinding userActivityBinding;
    private final String TAG=LoginActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
     //   setUpObserver(userViewModel);
    }
    private void initDataBinding() {
        userActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        userViewModel = new UserViewModel(this,this);
        userActivityBinding.setViewModel(userViewModel);
      }


    @Override
    public void login() {
        String email = userActivityBinding.etEmail.getText().toString();
        String password = userActivityBinding.etPassword.getText().toString();
        if (userViewModel.isEmailAndPasswordValid(email, password)) {
            userViewModel.login(email, password);
        } else {
            Toast.makeText(this, getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userViewModel.reset();
    }
}
