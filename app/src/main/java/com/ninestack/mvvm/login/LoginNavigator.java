package com.ninestack.mvvm.login;

/**
 * Created by Adrian Almeida.
 */
public interface LoginNavigator {
    void login();

    void openMainActivity();
}
