package com.ninestack.mvvm.login;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ninestack.mvvm.AppController;
import com.ninestack.mvvm.R;
import com.ninestack.mvvm.login.UserModel;
import com.ninestack.mvvm.network.UsersService;
import com.ninestack.mvvm.util.CommonUtils;

import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Adrian Almeida.
 */
public class UserViewModel extends ViewModel {

    /*public ObservableInt progressBar;
    public ObservableInt userRecycler;
    public ObservableInt userLabel;
    public ObservableField<String> messageLabel;*/

    private UserModel userModel;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private LoginNavigator loginNavigator;

    public UserViewModel(@NonNull Context context, LoginNavigator loginNavigator) {
        this.context = context;
        this.userModel = new UserModel();
        this.loginNavigator = loginNavigator;
    }


    public void login(String email, String password) {

        AppController appController = AppController.create(context);
        UsersService usersService = appController.getUserService();


        userModel.setPassword(Base64.encodeToString(password.getBytes(), Base64.DEFAULT));
        userModel.setEmail(email);

        Disposable disposable = usersService.login(userModel)
                .subscribeOn(appController.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<UserModel>() {
                    @Override
                    public void accept(UserModel userResponse) throws Exception {
                        if (userModel != null && userModel.getResult().equalsIgnoreCase("success")) {
                            loginNavigator.openMainActivity();
                            Toast.makeText(context, "working", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });

        compositeDisposable.add(disposable);
    }


    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }


    public boolean isEmailAndPasswordValid(String email, String password) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;

    }


    public void onServerLoginClick() {
        Log.i("User View Model", "Clicked");
        loginNavigator.login();
    }
}
