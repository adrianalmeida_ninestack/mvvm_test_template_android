package com.ninestack.mvvm.util;

import android.util.Patterns;

/**
 * Created by Adrian Almeida.
 */
public class CommonUtils {

    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
